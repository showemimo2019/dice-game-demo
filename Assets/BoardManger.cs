using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BoardManger : MonoBehaviour
{
    [SerializeField]private BoardItem boardItemPrefab;
    [SerializeField]private Transform boardItemContentHolder;
    List<BoardItem>boardItems = new List<BoardItem>();
    List<int>noBucket =new List<int>();
    List<BoardItem>itemBucket =new List<BoardItem>();
    [SerializeField] ScrollRect rect;

    

    private void Awake() {
     
       

        for (int i = 0; i < 100; i++)
        {
            BoardItem item = Instantiate(boardItemPrefab,boardItemContentHolder);
            itemBucket.Add(item);
            noBucket.Add(i); 
        }
        SortBox();
    }

    private void SortBox()
    {
        int noOfGuess=0;

        while (noOfGuess < 50)
        {
            int guPoint = Random.Range(0,noBucket.Count);
            noBucket.RemoveAt(guPoint);
            noOfGuess++;
        }
        int i = 1;
        
       

        foreach (int boxID in noBucket)
        {
    
            itemBucket[boxID].EnableBox(rect,i,Random.Range(0,7));
            i++;
            
        }
           itemBucket[noBucket[0]].MakeCenter();
        
    }

    public void MoveToByStep(Transform avatar, int steps,int positionOnBoard)
    {
        Debug.Log(positionOnBoard+steps);
        avatar.SetParent( itemBucket[noBucket[positionOnBoard+steps]].transform);
        itemBucket[noBucket[positionOnBoard+steps]].MakeCenter();
        itemBucket[noBucket[positionOnBoard+steps]].OnEnter();
    
    }

    private IEnumerator MovePiece(int step ,Transform  avatar, int startPoint)
    {
        //var pace =  speed * Time.deltaTime; // calculate distance to move
        //avatar.translate ()//= Vector3.MoveTowards(  avatar.transform.position ,itemBucket[noBucket[startPoint]], pace);
        
        yield return null;

    }

    
}
