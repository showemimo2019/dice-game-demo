using System;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(BoxCollider),typeof(Rigidbody))]
public class Dice : MonoBehaviour
{
    private Rigidbody rb;
    [SerializeField]private List<Face> faces= new List<Face>();

    public float min,max;   
    Vector3 startPoint;
   public Action<int> onDiceRoll;


    private void Awake() {
        rb = GetComponent<Rigidbody>();
        startPoint = transform.position;
    }
   
   public  void Reset() {
    gameObject.SetActive(false);
    transform.position =startPoint;
    gameObject.SetActive(true);
    rb.AddTorque(new Vector3( UnityEngine.Random.Range(min,max), UnityEngine.Random.Range(min,max), UnityEngine.Random.Range(min,max)));
    rb.AddForce(Vector3.forward*UnityEngine.Random.Range(min,max));
    StartCoroutine(CheckDiceVale());
    
   }

    public  void Push() {
    rb.AddTorque(new Vector3( UnityEngine.Random.Range(10,50), UnityEngine.Random.Range(10,50), UnityEngine.Random.Range(10,50)));
    rb.AddForce(Vector3.forward*10);
    StartCoroutine(CheckDiceVale());
    
   }

   private IEnumerator CheckDiceVale()
   {
    Debug.Log("hhgh");
        yield return new WaitUntil(() => rb.IsSleeping() == true);
        Debug.Log("Sleeep");
        yield return new WaitForSeconds(0.7f);
        CheckValue(transform.rotation.eulerAngles);
    
   }



   private void CheckValue(Vector3 rotEuler)
   {
      foreach (Face item in faces)
      {
        if(item.CheckIsValue())
        {
            onDiceRoll?.Invoke(item.Value);
            Debug.Log(item.Value);
            return;
        }
        
      }
    Push();
     
     
   }

}


public static class UIExtensions {
    // Shared array used to receive result of RectTransform.GetWorldCorners
    static Vector3[] corners = new Vector3[4];
    private const int MaxScrollTimeSec = 2;
   private const float ScrollTimeStep = 0.25f;
    
    /// <summary>
    /// Transform the bounds of the current rect transform to the space of another transform.
    /// </summary>
    /// <param name="source">The rect to transform</param>
    /// <param name="target">The target space to transform to</param>
    /// <returns>The transformed bounds</returns>
    public static Bounds TransformBoundsTo(this RectTransform source, Transform target)
    {
        // Based on code in ScrollRect's internal GetBounds and InternalGetBounds methods
        var bounds = new Bounds();
        if (source != null) {
            source.GetWorldCorners(corners);

            var vMin = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
            var vMax = new Vector3(float.MinValue, float.MinValue, float.MinValue);

            var matrix = target.worldToLocalMatrix;
            for (int j = 0; j < 4; j++) {
                Vector3 v = matrix.MultiplyPoint3x4(corners[j]);
                vMin = Vector3.Min(v, vMin);
                vMax = Vector3.Max(v, vMax);
            }

            bounds = new Bounds(vMin, Vector3.zero);
            bounds.Encapsulate(vMax);
        }
        return bounds;
    }

    /// <summary>
    /// Normalize a distance to be used in verticalNormalizedPosition or horizontalNormalizedPosition.
    /// </summary>
    /// <param name="axis">Scroll axis, 0 = horizontal, 1 = vertical</param>
    /// <param name="distance">The distance in the scroll rect's view's coordiante space</param>
    /// <returns>The normalized scoll distance</returns>
    public static float NormalizeScrollDistance(this ScrollRect scrollRect, int axis, float distance)
    {
        // Based on code in ScrollRect's internal SetNormalizedPosition method
        var viewport = scrollRect.viewport;
        var viewRect = viewport != null ? viewport : scrollRect.GetComponent<RectTransform>();
        var viewBounds = new Bounds(viewRect.rect.center, viewRect.rect.size);

        var content = scrollRect.content;
        var contentBounds = content != null ? content.TransformBoundsTo(viewRect) : new Bounds();

        var hiddenLength = contentBounds.size[axis] - viewBounds.size[axis];
        return distance / hiddenLength;
    }

    /// <summary>
    /// Scroll the target element to the vertical center of the scroll rect's viewport.
    /// Assumes the target element is part of the scroll rect's contents.
    /// </summary>
    /// <param name="scrollRect">Scroll rect to scroll</param>
    /// <param name="target">Element of the scroll rect's content to center vertically</param>
    public static void ScrollToCeneter(this ScrollRect scrollRect, RectTransform target)
    {
        // The scroll rect's view's space is used to calculate scroll position
        var view = scrollRect.viewport != null ? scrollRect.viewport : scrollRect.GetComponent<RectTransform>();

        // Calcualte the scroll offset in the view's space
        var viewRect = view.rect;
        var elementBounds = target.TransformBoundsTo(view);
        var offset = viewRect.center.y - elementBounds.center.y;

        // Normalize and apply the calculated offset
        var scrollPos = scrollRect.verticalNormalizedPosition - scrollRect.NormalizeScrollDistance(1, offset);
        scrollRect.verticalNormalizedPosition = Mathf.Clamp(scrollPos, 0f, 1f);
    }
      public static IEnumerator VerticalNormalizedPositionSmooth(this ScrollRect scrollRect, float position)
        {
            var maxTime = DateTime.Now.AddSeconds(MaxScrollTimeSec).Second;

            while (true)
            {
                scrollRect.verticalNormalizedPosition = Mathf.Lerp(scrollRect.verticalNormalizedPosition, position, ScrollTimeStep);
                yield return new WaitForEndOfFrame();

                var pos1 = Mathf.Round(scrollRect.verticalNormalizedPosition * 1000.0f) * 0.001f;
                var pos2 = Mathf.Round(position * 1000.0f) * 0.001f;

                if (pos1 == pos2 || maxTime <= DateTime.Now.Second)
                {
                    scrollRect.verticalNormalizedPosition = position;

                    yield break;
                }
            }
        }
}
