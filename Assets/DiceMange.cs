using System.Collections;
using UnityEngine.UI;
using UnityEngine;

public class DiceMange : MonoBehaviour
{
    [SerializeField]private Dice dice;
    [SerializeField]private Image touchImage;
    Button rollBtn;
    bool isMouseToRoll = false;

    private void Awake() {
      rollBtn =  touchImage.GetComponent<Button>();
      rollBtn.onClick.AddListener(() =>RollDice());
      dice.onDiceRoll+=ResetDice;
    }

    private void RollDice()
    {
        dice.Reset();
        touchImage.gameObject.SetActive(false);

    }
    private void ResetDice(int x)
    {
      touchImage.gameObject.SetActive(true);

    }



    

}
