using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(Button))]
public class SceneLoader : MonoBehaviour
{
    [SerializeField]int sceneIndex;

    private void Awake() {
        GetComponent<Button>().onClick.AddListener(() =>LoadSceneIndex());
    }

    private void LoadSceneIndex()
    {
           SceneManager.LoadScene(sceneIndex, LoadSceneMode.Single);

    }


}
