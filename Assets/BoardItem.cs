using System;
using UnityEngine.UI;
using UnityEngine;
using TMPro;

public class BoardItem : MonoBehaviour
{
    [SerializeField] Image bg;
    [SerializeField] TextMeshProUGUI no;
     ScrollRect rect;
     BoxEffect effect;
     

    

    public void EnableBox(ScrollRect rect,int pos,int boardEffect) {
        this.rect = rect;
        bg.enabled = true;
        
        no.text = pos.ToString();
        no.enabled = true;
        if (pos ==1)
        {
            bg.color= Color.green;
            effect = BoxEffect.Neutral;
            

        }
        else
        {
            
            switch (boardEffect)
            {
                case 1:
                bg.color= Color.blue;
                effect = BoxEffect.MoveForward;
                break;
    
    
                case 2:
                bg.color= Color.red;
                effect = BoxEffect.MoveBack;
    
    
                break; 
                
                default:
                bg.color= Color.green;
                effect = BoxEffect.Neutral;
                break;
            }

        }

    }
    
   public void MakeCenter()
   {
      rect.ScrollToCeneter((RectTransform)transform);
     
   }
   public void OnEnter()
   {
    switch (effect)
    {
        case BoxEffect.MoveBack:
    PlayerManager.instance.MovePoint(-1);
        break;

        case BoxEffect.MoveForward:
    PlayerManager.instance.MovePoint(1);
        break;

        
        default:
        break;
    }

   }
   
}

public enum BoxEffect
{
    Neutral,
    MoveBack,
    MoveForward,

}