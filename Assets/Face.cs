using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Face : MonoBehaviour
{
    public LayerMask layerMask;
    [SerializeField]private int value;
    bool isFaceUp;
    public int Value{get{return value;}}
    private void Update() {

        Ray ray  = new Ray(transform.position,transform.forward);
         if (Physics.Raycast(ray,layerMask))
        {
            Debug.DrawRay(transform.position,transform.forward,Color.red);

           
            isFaceUp = false;
        }
        else
        {
            isFaceUp = true;
            Debug.DrawRay(transform.position,transform.forward,Color.green);

        }
    }


    public bool CheckIsValue() =>isFaceUp;
}


