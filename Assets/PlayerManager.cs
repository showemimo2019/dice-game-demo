using UnityEngine;
using UnityEngine.UI;

public class PlayerManager : MonoBehaviour
{
    [SerializeField]private Dice dice;
    [SerializeField]private Image avatar,winUI;
    [SerializeField]private BoardManger board;
    public static PlayerManager instance;
    int position=0;

    
    

    private void Awake() {
        if(instance != null)
        Destroy(instance);
       dice.onDiceRoll +=StartMoveMent;
       // dice.onDiceRoll +=MovePlayer;
        avatar.enabled = false;
        instance = this;
    }

    public void MovePoint(int step)
    {
        MovePlayer(step);
        
    }

    
    private void StartMoveMent(int sixRoll)
    {
        if(sixRoll == 6)
        {
            dice.onDiceRoll -= StartMoveMent;
            dice.onDiceRoll +=MovePlayer;
            avatar.enabled = true;
            board.MoveToByStep(avatar.transform,0,position);
          // Debug.Log("start");

        }
        //else
     //   Debug.Log("Cant move yet");

    }
    private void MovePlayer(int steps)
    {
       // Debug.Log("Move "+steps +" steps" );
        if((position+steps) <= 49)
        {
            board.MoveToByStep(avatar.transform,steps,position);
            position+=steps;
             if(position == 49)
                winUI.gameObject.SetActive(true);

        }
       // else
       // Debug.Log("Invalid roll");

    }
}
